package com.jobgroup6;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

public class Gene {
    
    //chromosome length
    int length;
    //looks like this: [2,1,3,5,4,6,0,6,3,4,5,2,1,0]
    //this chromosome describes two resources and 6 jobs
    //the first 6 figures describe the job order of resource one
    //the next 6 figures describe the job order of resource two
    int[] chromosome;
    //makespan
    int fitness = 0;

    public Gene(int length) {
        this.length = length;
        this.chromosome = new int[length];
    }

    public Gene(int machines, int jobs, Solution solution) {
        this.length = machines * jobs;
        this.chromosome = new int[this.length];

        int chromosomeIterator = 0;

        //translate schedule to chromosome
        for(Map.Entry<Integer, LinkedList<int[]>> entry : solution.schedule.entrySet()) {
            int resourceId = entry.getKey();
            LinkedList<int[]> scheduledOperations = entry.getValue();

            for(int i = chromosomeIterator; i<(chromosomeIterator + scheduledOperations.size()); i++) {
                this.chromosome[i] = scheduledOperations.get(i-chromosomeIterator)[0];
                
            }
            chromosomeIterator+=jobs;
        }
    }

    public void displaySolution(int resources, int jobs) {

        int chromosomeCounter = 0;

        for(int i = 0; i<resources; i++) {
            System.out.println("ResourceId: " + i);
            
            for(int j = 0; j<jobs; j++) {
                System.out.println("-> " + this.chromosome[j+chromosomeCounter]);
            }

            chromosomeCounter += jobs;
        }
    }

    public void displayChromosome() {
        System.out.println("");
        System.out.print("[");
        for(int i = 0; i<chromosome.length; i++) {

            System.out.print(this.chromosome[i]);
            
            if(i<chromosome.length-1) {
                System.out.print(", ");
            }
            
        }
        System.out.print("]");
        System.out.println("");
        System.out.println("");
    }
}
