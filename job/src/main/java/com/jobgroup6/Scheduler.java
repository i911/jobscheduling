package com.jobgroup6;

//schedules an initial solution with chosen heuristic
public class Scheduler {

    Heuristic heuristic = new Heuristic();
    Solution solution = new Solution();
    
    public Solution schedule(Problem problem) {
        
        //select heuristic here
        solution.schedule = heuristic.random(problem);
        
        return solution;
    }
}
