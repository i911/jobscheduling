package com.jobgroup6;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.stream.Collectors;

import com.google.gson.Gson;

public class Reader {
    
    public Problem read() throws FileNotFoundException {

        BufferedReader file = new BufferedReader(new FileReader("/Users/Jendrik/Downloads/benchmark_problems/benchmark_problems/la32_30_jobs_10_resources.json"));
        String str = file.lines().collect(Collectors.joining());

        Gson gson = new Gson();

        Problem problem = gson.fromJson(str, Problem.class);

        for(int i = 0; i<problem.jobs.size(); i++) {
            problem.jobs.get(i).setJobIdForOperations();
        }
        
        return problem;
    }
}
