package com.jobgroup6;

import java.util.LinkedList;


//class holding the json data
public class Problem {
    
    public LinkedList<Resource> resources;
    public LinkedList<Job> jobs;

    boolean scheduled = false;

    //copy the problem with different objects
    Problem(Problem anotherProblem) {

        resources = new LinkedList<>();
        jobs = new LinkedList<>();
        
        for(int i = 0; i<anotherProblem.resources.size(); i++) {
            Resource resource = new Resource();
            resource.id = anotherProblem.resources.get(i).id;
            this.resources.add(resource);
        }

        for(int i = 0; i<anotherProblem.jobs.size(); i++) {
            Job job = new Job();
            job.id = anotherProblem.jobs.get(i).id;
            job.operations = new LinkedList<>();

            for(int j = 0; j<anotherProblem.jobs.get(i).operations.size(); j++) {
                Operation operation = new Operation();
                operation.index = anotherProblem.jobs.get(i).operations.get(j).index;
                operation.duration = anotherProblem.jobs.get(i).operations.get(j).duration;
                operation.resource = anotherProblem.jobs.get(i).operations.get(j).resource;
                job.operations.add(operation);
            }
            this.jobs.add(job);
        }
    }
}
