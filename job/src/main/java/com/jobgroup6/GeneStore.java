package com.jobgroup6;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Random;

public class GeneStore {
    
    LinkedList<Gene> genes = new LinkedList<>();
    LinkedList<Gene> newPopulation = new LinkedList<>();
    LinkedList<Integer> fitness = new LinkedList<>();
    Problem problem;
    HashMap<Integer, HashMap<Integer, LinkedList<int[]>>> initialSolutions = new HashMap<>();


    GeneStore(Problem problem) {
        this.problem = problem;
    }

    
    //calculates the fitness of the gene
    public void calculateFitness(Gene gene) {

        //new objects to create an independent copy of the problem
        LinkedList<Resource> resources = new LinkedList<>();
        LinkedList<Job> jobs = new LinkedList<>();

        //instantiate resources copy
        for(int i = 0; i<this.problem.resources.size(); i++) {
            Resource resource = new Resource();
            resource.id = i;
            resources.add(resource);
        }

        //instantiate jobs copy
        for(int i = 0; i<this.problem.jobs.size(); i++) {
            Job job = new Job();
            job.id = i;
            
             for(int j = 0; j<this.problem.jobs.get(i).operations.size(); j++) {
                 Operation operation = new Operation();
                 operation.index = j;
                 operation.jobID = i;
                 operation.resource = this.problem.jobs.get(i).operations.get(j).resource;
                 operation.duration = this.problem.jobs.get(i).operations.get(j).duration;

                 job.operations.add(operation);
             }

            jobs.add(job);
        }

        //fill schedule by iterating through chromosome.
        //the resources will be iterated and by using the job id and making use of the 
        //chromosome order (resources schedule in ascending order by id) the jobs will be added to the schedule
        HashMap<Integer, LinkedList<int[]>> schedule = new HashMap<>();

        int[] chromosome = gene.chromosome;

        int chromosomeCounter = 0;

        for(int i = 0; i<resources.size(); i++) {

            schedule.put(i, new LinkedList<int[]>());

            Resource resource = resources.get(i);

            for(int j = chromosomeCounter; j<chromosomeCounter+jobs.size(); j++) {
                Job job = jobs.get(chromosome[j]);

                for(int k = 0; k<job.operations.size(); k++) {
                    if(job.operations.get(k).resource == resource.id) {

                        LinkedList<int[]> operations = schedule.get(i);
                        operations.add(new int[]{job.id, job.operations.get(k).index});
                        schedule.put(i, operations);
                    }
                }
            }

            chromosomeCounter+=jobs.size();
        
        }

        //add the scheduled operations to the resources
        for(Map.Entry<Integer, LinkedList<int[]>> entry : schedule.entrySet()) {

           

            int resourceId = entry.getKey();
            LinkedList<int[]> listOfOperations = entry.getValue();

            Resource resource = resources.get(resourceId);

            for(int i = 0; i<listOfOperations.size(); i++) {
                int[] operationIndex = listOfOperations.get(i);
                Job job = jobs.get(operationIndex[0]);
                Operation operation = job.operations.get(operationIndex[1]);

                resource.scheduledOperations.add(operation);
            }
        }

        //schedule all operations minding the time to get the complete makespan of the job (incl. waiting time)
        int numberOfScheduledJobs = 0;
        //variable to filter invalid solutions
        int iterationsWithNoChange = 0;

        //do while not every job is scheduled
        while(numberOfScheduledJobs < jobs.size()) {

        //cancel the while loop if there are two iterations without a change to the jobs
        //fitness of the gene will be set to 0 to filter this invalid solution during tournament selection
        if(iterationsWithNoChange >= 2) {
            gene.fitness = 0;
            return;
        }

        //this variable will be set to true if an operation was scheduled
        boolean change = false;

        //iterate throught the resources
        for(int i = 0; i<resources.size(); i++) {

            Resource currentResource = resources.get(i);
            LinkedList<Operation> operations = currentResource.scheduledOperations;

            //iterate through the assigned operations of that resource
            for(int j = 0; j<operations.size(); j++) {

                Operation currentOperation = operations.get(j);
                Job job = jobs.get(currentOperation.jobID);

                if(currentOperation.scheduled) {
                    continue;
                }

                //check if the previous  operations are scheduled
                if(job.previousOperationsScheduled(currentOperation).previouslyScheduled) {

                    if(currentOperation.index == 0) {
                        
                        currentOperation.scheduled = true;
                        currentOperation.startTime = currentResource.waitingTime + currentResource.workingTime;
                        currentOperation.endTime = currentOperation.startTime + currentOperation.duration;
                        currentResource.workingTime = currentResource.workingTime + currentOperation.duration;

                        change = true;
                        
                        continue;
                    }
                    
                    Operation previousOperation = job.operations.get(currentOperation.index-1);

                    //if the previous operation ends after the resource is ready to work again, add waiting time
                    if(previousOperation.endTime > (currentResource.workingTime + currentResource.waitingTime)) {

                        int waitingTime = previousOperation.endTime - (currentResource.workingTime + currentResource.waitingTime);
                        currentResource.waitingTime = currentResource.waitingTime + waitingTime;
                        currentOperation.startTime = currentResource.waitingTime + currentResource.workingTime;
                    }
                    //if the previous operation ends before the machine is ready to work again, schedule the operation right when the resource is ready
                    else if(previousOperation.endTime < (currentResource.workingTime + currentResource.waitingTime)) {
                        currentOperation.startTime  = currentResource.waitingTime + currentResource.workingTime;
                    }
                    else {
                        currentOperation.startTime = previousOperation.endTime;
                    }

                    //set working time of resource and endtime of operation
                    currentResource.workingTime = currentResource.workingTime + currentOperation.duration;
                    currentOperation.scheduled = true;
                    currentOperation.endTime = currentOperation.startTime + currentOperation.duration;

                    change = true;

                    //if last operation of the job is scheduled increase number of scheduled jobs by 1
                    if(currentOperation.index == job.operations.size()-1) {
                        numberOfScheduledJobs++;
                        job.endTime = job.operations.getLast().endTime;
                    }
                } 
                else {
                    break;
                }
            }
        }

        //if no change occured, increase the variable
        if(!change) {
            iterationsWithNoChange++;
        }
        
    }

    //take the highest makespan of the jobs and return it as the complete makespan of the gene
    int completeMakespan = 0;

    for(int i = 0; i<jobs.size(); i++) {
        if(jobs.get(i).endTime > completeMakespan) {
            completeMakespan = jobs.get(i).endTime;
        }
    }
    
    gene.fitness = completeMakespan;
    
    }

    //do a tournament selection with a tournament size of 2 and return the winners
    public LinkedList<Gene> tournamentSelection() {

        this.newPopulation.clear();
        this.fitness.clear();

        //if the number of genes competing is uneven, generate an additional solution
        if(this.genes.size()%2 != 0 ) {
            Solution additionalSolution = new Solution();
            Heuristic heuristic = new Heuristic();
            additionalSolution.schedule = heuristic.random(this.problem);

            Gene additionalGene = new Gene(this.problem.resources.size(), this.problem.jobs.size(), additionalSolution);
            this.genes.add(additionalGene);
        }

        //gene pool with competing genes
        LinkedList<Gene> genePool = new LinkedList<>();

        for(int i = 0; i<this.genes.size(); i++) {
            genePool.add(this.genes.get(i));
        }


        if(this.genes.size() > 0) {

            
            //list with winners
            LinkedList<Gene> winners = new LinkedList<>();

            while(genePool.size() > 0) {

               //get two random genes
                int randomOne = (int)(Math.random() * (genePool.size()-1));
                int randomTwo = genePool.size()+1;

                //make sure they are not the same
                while(randomTwo == genePool.size()+1 || randomTwo == randomOne) {
                    randomTwo = (int)(Math.random() * (genePool.size()));
                }

                Gene competingGeneOne = genePool.get(randomOne);
                Gene competingGeneTwo = genePool.get(randomTwo);


                //gene 2 fitness value needs to be zero or gene 1 fitness unequal to zero to fulfill the first argument
                //then the first gene also needs to have a lower fitness value
                if( (competingGeneTwo.fitness == 0 || competingGeneOne.fitness != 0) && competingGeneOne.fitness < competingGeneTwo.fitness) {
                    winners.add(competingGeneOne);
                    
                    this.genes.remove(competingGeneTwo);
                }
                //other way around
                else if((competingGeneOne.fitness == 0 || competingGeneTwo.fitness != 0) && competingGeneTwo.fitness < competingGeneOne.fitness){
                    winners.add(competingGeneTwo);
                    
                    this.genes.remove(competingGeneOne);
                }
                //both fitness values are unequal to zero, but are the same -> add both to winners
                else if((competingGeneOne.fitness != 0 && competingGeneTwo.fitness != 0) && competingGeneTwo.fitness == competingGeneOne.fitness) {
                    winners.add(competingGeneTwo);
                    winners.add(competingGeneOne);
                    
                }
                else if(competingGeneTwo.fitness == 0 && competingGeneOne.fitness != 0) {
                    winners.add(competingGeneOne);
                    
                    this.genes.remove(competingGeneTwo);
                }
                else if(competingGeneTwo.fitness != 0 && competingGeneOne.fitness == 0) {
                    winners.add(competingGeneTwo);
                    
                    this.genes.remove(competingGeneOne);
                }
                //if both are zero, both solutions need to be filtered
                else {
                    System.out.println("x");
                }

                //remove the genes that already competed
                genePool.remove(competingGeneOne);
                genePool.remove(competingGeneTwo);
  
            }


            
            return winners;
        }

        //if there are only two genes in the pool, return both
        else if(this.genes.size() == 2) {
            LinkedList<Gene> result = new LinkedList<>();
            result.add(this.genes.get(0));
            result.add(this.genes.get(1));
            
            return result;
        }
        else {
            return null;
        }

    }

    //this method is to return a single child
    //used to fill up the population until a specific size is reached
    public Gene singleTournament(Gene competingGeneOne, Gene competingGeneTwo) {

                if( (competingGeneTwo.fitness == 0 || competingGeneOne.fitness != 0) && competingGeneOne.fitness < competingGeneTwo.fitness) {
                    return competingGeneOne;
                }
                else if((competingGeneOne.fitness == 0 || competingGeneTwo.fitness != 0) && competingGeneTwo.fitness < competingGeneOne.fitness){
                    return competingGeneTwo;
                }
                else if(competingGeneTwo.fitness == 0 && competingGeneOne.fitness != 0) {
                    return competingGeneOne;
                }
                else if(competingGeneTwo.fitness != 0 && competingGeneOne.fitness == 0) {
                   return competingGeneTwo;
                }
                else {
                    int random = (int)(Math.random() * 2) + 1;

                    if(random == 1) {
                        return competingGeneOne;
                    }
                    else {
                        return competingGeneTwo;
                    }
                }
    }

    //crossover recombination with two genes
    public LinkedList<Gene> crossOverRecombination(Gene firstGene, Gene secondGene) {


        //first and second half of first gene
        int[] firstHalfFirstGene;
        int[] SecondHalfFirstGene;

        //first and second half of second gene
        int[] firstHalfSecondGene;
        int[] SecondHalfSecondGene;


        //if number of resources is uneven, then the lengths of the two chromosome parts are different
        if(this.problem.resources.size()%2 != 0 ) {

            //first part is shorter
            int partlengthOne = this.problem.jobs.size() * (this.problem.resources.size()/2);
            //second part holds the jobs for an additional resource
            int partlengthTwo = partlengthOne + problem.jobs.size();

            firstHalfFirstGene = new int[partlengthOne];
            SecondHalfFirstGene = new int[partlengthTwo];

            firstHalfSecondGene = new int[partlengthOne];
            SecondHalfSecondGene = new int[partlengthTwo];
        }
        //if the number of resources is even, the lengths are the same
        else {
            
            int partlength = (this.problem.resources.size()/2) * this.problem.jobs.size();

            firstHalfFirstGene = new int[partlength];
            SecondHalfFirstGene = new int[partlength];

            firstHalfSecondGene = new int[partlength];
            SecondHalfSecondGene = new int[partlength];
        }
        
        //fill the first half of the first gene
        for(int i = 0; i<firstHalfFirstGene.length; i++) {
            firstHalfFirstGene[i] = firstGene.chromosome[i];
        }
        //fill the second half of the first gene
        for(int i = firstHalfFirstGene.length; i<(firstHalfFirstGene.length + SecondHalfFirstGene.length); i++) {
            SecondHalfFirstGene[i-firstHalfFirstGene.length] = firstGene.chromosome[i];
        }
        //fill the first half of the second gene
        for(int i = 0; i<firstHalfSecondGene.length; i++) {
            firstHalfSecondGene[i] = secondGene.chromosome[i];
        }
        //fill the second half of the second gene
        for(int i = firstHalfSecondGene.length; i<(firstHalfSecondGene.length + SecondHalfSecondGene.length); i++) {
            SecondHalfSecondGene[i-firstHalfSecondGene.length] = secondGene.chromosome[i];
        }

        //create the child genes with the given chromosome length
        Gene childOne = new Gene(firstHalfFirstGene.length+SecondHalfFirstGene.length);
        Gene childTwo = new Gene(firstHalfSecondGene.length+SecondHalfSecondGene.length);

        //add first half of first gene to first child
        for(int i = 0; i<firstHalfFirstGene.length; i++) {
            childOne.chromosome[i] = firstHalfFirstGene[i];
        }
        //add second half of second gene to first child
        for(int i = 0; i<SecondHalfSecondGene.length; i++) {
            childOne.chromosome[i+firstHalfFirstGene.length] = SecondHalfSecondGene[i];
        }
        //add first half of second gene to second child
        for(int i = 0; i<firstHalfSecondGene.length; i++) {
            childTwo.chromosome[i] = firstHalfSecondGene[i];
        }
        //add second half of first gene to second child
        for(int i = 0; i<SecondHalfFirstGene.length; i++) {
            childTwo.chromosome[i+firstHalfSecondGene.length] = SecondHalfFirstGene[i];
        }

        LinkedList<Gene> children = new LinkedList<>();

        children.add(childOne);
        children.add(childTwo);

        //calculate the children's fitness
        this.calculateFitness(childOne);
        this.calculateFitness(childTwo);

        return children;
    }

    public LinkedList<Gene> AlternativeCrossOverRecombination(Gene firstGene, Gene secondGene) {


        //first and second half of first gene
        int[] firstHalfFirstGene;
        int[] SecondHalfFirstGene;

        //first and second half of second gene
        int[] firstHalfSecondGene;
        int[] SecondHalfSecondGene;


        //if number of resources is uneven, then the lengths of the two chromosome parts are different
        

            //first part is shorter
            int partlengthOne = this.problem.jobs.size() * (this.problem.resources.size()-1);
            //second part holds the jobs for an additional resource
            int partlengthTwo = problem.jobs.size();

            firstHalfFirstGene = new int[partlengthOne];
            SecondHalfFirstGene = new int[partlengthTwo];

            firstHalfSecondGene = new int[partlengthOne];
            SecondHalfSecondGene = new int[partlengthTwo];
        
        
        //fill the first half of the first gene
        for(int i = 0; i<firstHalfFirstGene.length; i++) {
            firstHalfFirstGene[i] = firstGene.chromosome[i];
        }
        //fill the second half of the first gene
        for(int i = firstHalfFirstGene.length; i<(firstHalfFirstGene.length + SecondHalfFirstGene.length); i++) {
            SecondHalfFirstGene[i-firstHalfFirstGene.length] = firstGene.chromosome[i];
        }
        //fill the first half of the second gene
        for(int i = 0; i<firstHalfSecondGene.length; i++) {
            firstHalfSecondGene[i] = secondGene.chromosome[i];
        }
        //fill the second half of the second gene
        for(int i = firstHalfSecondGene.length; i<(firstHalfSecondGene.length + SecondHalfSecondGene.length); i++) {
            SecondHalfSecondGene[i-firstHalfSecondGene.length] = secondGene.chromosome[i];
        }

        //create the child genes with the given chromosome length
        Gene childOne = new Gene(firstHalfFirstGene.length+SecondHalfFirstGene.length);
        Gene childTwo = new Gene(firstHalfSecondGene.length+SecondHalfSecondGene.length);

        //add first half of first gene to first child
        for(int i = 0; i<firstHalfFirstGene.length; i++) {
            childOne.chromosome[i] = firstHalfFirstGene[i];
        }
        //add second half of second gene to first child
        for(int i = 0; i<SecondHalfSecondGene.length; i++) {
            childOne.chromosome[i+firstHalfFirstGene.length] = SecondHalfSecondGene[i];
        }
        //add first half of second gene to second child
        for(int i = 0; i<firstHalfSecondGene.length; i++) {
            childTwo.chromosome[i] = firstHalfSecondGene[i];
        }
        //add second half of first gene to second child
        for(int i = 0; i<SecondHalfFirstGene.length; i++) {
            childTwo.chromosome[i+firstHalfSecondGene.length] = SecondHalfFirstGene[i];
        }

        LinkedList<Gene> children = new LinkedList<>();

        children.add(childOne);
        children.add(childTwo);

        //calculate the children's fitness
        this.calculateFitness(childOne);
        this.calculateFitness(childTwo);

        return children;
    }

    //insert mutation with mutation rate
    public void mutation(Gene gene) {

        //mutation rate
        double mutationRate = 0.05;

        //for iterating through the chromosome minding the fields for the resources (e.g. first 10 fields for the job order of resource one)
        int chromosomeCounter = 0;

        //iterate through the resources
        for(int i = 0; i<problem.resources.size(); i++) {
            
            double random = new Random().nextDouble();

            //if random value is smaller of the mutation rate then mutate
            if(random <= mutationRate) {
                int randomJobOne = ((int)(Math.random() * problem.jobs.size())) + chromosomeCounter;
                int randomJobTwo = ((int)(Math.random() * problem.jobs.size())) + chromosomeCounter;

                while(randomJobTwo == randomJobOne) {
                    randomJobTwo = ((int)(Math.random() * problem.jobs.size()))+chromosomeCounter;
                }

                int tmp = gene.chromosome[randomJobOne];
                gene.chromosome[randomJobOne] = gene.chromosome[randomJobTwo];
                gene.chromosome[randomJobTwo] = tmp;

            }

            chromosomeCounter += problem.jobs.size();
        }

        //calculate the new fitness
        this.calculateFitness(gene);
    }

    //generate new generation
    public void newGeneration(int populationSize) {
        
        //get the parents by tournament selection
        LinkedList<Gene> parents = this.tournamentSelection();

        //make single tournament until the size is half the population
        while(this.genes.size() > populationSize/2) {

            int randomOne = (int)(Math.random() * parents.size());
            int randomTwo = (int)(Math.random() * parents.size());

            while(randomTwo == randomOne) {
                randomTwo = (int)(Math.random() * parents.size());
            }

            Gene geneOne = parents.get(randomOne);
            Gene geneTwo = parents.get(randomTwo);

            parents.remove(geneOne);
            parents.remove(geneTwo);

            genes.remove(geneOne);
            genes.remove(geneTwo);

            Gene survivor = this.singleTournament(geneOne, geneTwo);

            parents.add(survivor);
        }

        LinkedList<Gene> children = new LinkedList<>();

        System.out.println(parents.size());

        int uselessChildren = 0;

        //generate children until the population size is reached
        while(children.size() < (populationSize-parents.size())) {

            int randomOne = (int)(Math.random() * parents.size());
            int randomTwo = (int)(Math.random() * parents.size());

            while(randomTwo == randomOne) {
                randomTwo = (int)(Math.random() * parents.size());
            }

            LinkedList<Gene> newChildren = new LinkedList<>();

            if(uselessChildren < 150) {
                newChildren = this.crossOverRecombination(parents.get(randomOne), parents.get(randomTwo));
            }
            else {
                newChildren = this.AlternativeCrossOverRecombination(parents.get(randomOne), parents.get(randomTwo));
                
            }

            /*parents.get(randomOne).displayChromosome();
            parents.get(randomTwo).displayChromosome();
            newChildren.getLast().displayChromosome();*/

            this.mutation(newChildren.getFirst());
            this.mutation(newChildren.getLast());

            //if the child has a fitness of 0, the solution is invalid (dead lock) and needs to be filtered
            if(newChildren.getFirst().fitness != 0 ) {
                children.add(newChildren.getFirst());
            }
            else {
                uselessChildren++;
            }
            if(newChildren.getLast().fitness != 0 ) {
                children.add(newChildren.getLast());
            }
            else {
                uselessChildren++;
            }
            
        }

        //add children to new population
        for(int i= 0; i<children.size(); i++) {
            this.newPopulation.add(children.get(i));

            this.fitness.add(children.get(i).fitness);
        }
        for(int i= 0; i<parents.size(); i++) {
            this.newPopulation.add(parents.get(i));

            this.fitness.add(parents.get(i).fitness);
        }

        //wipe out old population
        genes.clear();
        //add new population
        genes.addAll(newPopulation);
    }

    public void addGeneToStore(Gene gene) {
        this.calculateFitness(gene);
        this.genes.add(gene);
        this.fitness.add(gene.fitness);
    }
}
