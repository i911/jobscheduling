package com.jobgroup6;

import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import javax.swing.plaf.basic.BasicTreeUI.TreeCancelEditingAction;

import com.jobgroup6.Job.ReturnType;

public class Heuristic {
    
    //chooses random operations and checks if they can be scheduled to minimize risk of dead lock
    public HashMap<Integer, LinkedList<int[]>> random(Problem problem) {

        //int{jobId, OperationIndex, resourceId}
        LinkedList<int[]> operations = new LinkedList<>();
        HashMap<Integer, LinkedList<int[]>> result = new HashMap<>();

        for(int i = 0; i<problem.jobs.size(); i++) {
            for(int j = 0; j<problem.jobs.get(i).operations.size(); j++) {
               
                int jobId = problem.jobs.get(i).id;
                int operationIndex = problem.jobs.get(i).operations.get(j).index;
                int resourceId = problem.jobs.get(i).operations.get(j).resource;
                int[] operation = new int[]{jobId, operationIndex, resourceId};
                operations.add(operation);
            }
        }

        
        while(!operations.isEmpty()) {

            int randomIndex = this.randomInt(operations.size());
            int[] currentOperation = operations.get(randomIndex);
            Job currentJob = problem.jobs.get(currentOperation[0]);
            ReturnType ret = currentJob.previousOperationsScheduled(currentJob.operations.get(currentOperation[1]));

            if(ret.previouslyScheduled || currentOperation[1] == 0) {

                LinkedList<int[]> newList = new LinkedList<>();

                //if result contains resourceId
                if(result.containsKey(currentOperation[2])) {
                    newList = result.get(currentOperation[2]);
                }
                else {
                    newList = new LinkedList<>();
                }
                //create new entry as int[jobId, operationIndex]
                int[] newEntry = new int[]{currentOperation[0], currentOperation[1]};
                //add entry to list
                newList.add(newEntry);
                //overwrite old value
                result.put(currentOperation[2], newList);
                problem.jobs.get(currentOperation[0]).operations.get(currentOperation[1]).scheduled = true;
                operations.remove(randomIndex);
                }
        }
        
        this.unschedule(problem);
        return result;
    }

    public HashMap<Integer, LinkedList<int[]>> SPT(Problem problem) {
        Problem problemClone = new Problem(problem);
        LinkedList<int[]> jobSchedule = new LinkedList<>();
        int scheduledJobs = 0;
        Job.ReturnType previousOperations = null;
        HashMap<Integer, LinkedList<int[]>> result = new HashMap<>();
        LinkedList<Job> jobs = problemClone.jobs;
        int countIter = 0;

        List<Operation> allOperationsList = new LinkedList<>();

        for (int i = 0; i < jobs.size(); i++) {
            LinkedList<Operation> ops = jobs.get(i).operations;
            for (int j = 0; j < ops.size(); j++) {
                //NEED jobIDs to know which operation belongs to which job!
                ops.get(j).jobID = jobs.get(i).id;
                allOperationsList.add(ops.get(j));
            }
        }

        //sort list by operation durations
        Collections.sort(allOperationsList, new Comparator<Operation>() {
            public int compare(Operation o1, Operation o2) {
                return Integer.compare(o1.duration, o2.duration);
            }
        });

        int shortestAvailable = 0;

        while (!problem.scheduled) {
            Operation shortestOp = allOperationsList.get(shortestAvailable);
            Job opJob = jobs.get(shortestOp.jobID);
            previousOperations = opJob.previousOperationsScheduled(shortestOp);
            //check if previous operation is finished - else take next shortest operation and repeat (prevent deadlocks)
            if (shortestOp.index != 0) {
                if (!previousOperations.previouslyScheduled) {
                    //start over with next operation, because waiting could potentially lead to a deadlock
                    shortestAvailable++;
                    continue;
                }
            }
            //result is: resource, job.id u. operation index (in einer Liste als Array)
            //now check if the machine needs to wait
            Resource resource = problem.resources.get(shortestOp.resource);
            

            jobSchedule.add(new int[]{opJob.id, shortestOp.index});
            if (!result.containsKey(resource.id)) {
                result.put(resource.id, new LinkedList<int[]>());
            }
            result.get(resource.id).add(jobSchedule.get(countIter));
            //put operation as scheduled
            shortestOp.scheduled = true;
            //problem.resources.get(resource.id).scheduledOperations.add(shortestOp);

            //check if shortestOperation was the last operation of a job
            if (jobs.get(opJob.id).operations.getLast() == shortestOp) {
                problem.jobs.get(opJob.id).scheduled = true;
                scheduledJobs++;
                //remove scheduled job from iteration list
                jobs.remove(problem.jobs.get(opJob.id));
            }

            if (scheduledJobs == problem.jobs.size()) {
                problem.scheduled = true;
            }
            allOperationsList.remove(shortestOp);
            //reset the shortest available operation to the first in the list
            shortestAvailable = 0;
            //increase counter for result input in next iteration
            countIter++;

        }

        problem.scheduled = false;
        this.unschedule(problem);
        return result;

    }

    public int randomInt(int size) {
        int result = (int)(Math.random() * (size-1));
        return result;
    }

    public void unschedule(Problem problem) {
        for(int i = 0; i<problem.jobs.size(); i++) {

            for(int j = 0; j<problem.jobs.get(i).operations.size(); j++) {
                problem.jobs.get(i).operations.get(j).scheduled = false;
            } 
        }
    }
}
