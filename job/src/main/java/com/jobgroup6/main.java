package com.jobgroup6;
import java.io.FileNotFoundException;
import java.util.Collections;



public class main {

    public static void main(String[] args) {

        Problem problem;
        
        //import file
        try {
            Reader reader = new Reader();
            problem = reader.read();

            Scheduler scheduler = new Scheduler();      

            GeneStore store = new GeneStore(problem);
            
            //initial solutions
            int numbersOfInitialSolutions = 100;

            for(int i = 0; i<numbersOfInitialSolutions; i++) {
                Solution solution = scheduler.schedule(problem);
                
                Gene gene = new Gene(problem.resources.size(), problem.jobs.size(), solution);
                store.addGeneToStore(gene);
                
                //gene.displayChromosome();
                System.out.println(gene.fitness);
            }

            System.out.println("Searching for valid children...");

            //evolutionary algorithm
            int generations = 100;
            for(int i = 0; i<generations; i++) {
                System.out.println();
                System.out.println("Generation: " + i);
                
                store.newGeneration(100);

                Collections.sort(store.fitness);

                //average fitness of the generation
                int sum = 0;
                for(int j =0; j<store.fitness.size(); j++) {
                
                    sum += store.fitness.get(j);
                    
                }

                System.out.println("Avg fitness: " + (sum/store.fitness.size()));

               
            }

            //sort the list
            Gene bestGene = store.genes.getFirst();

            for(int i = 0; i<store.genes.size(); i++) {
                if(store.genes.get(i).fitness != 0 && store.genes.get(i).fitness < bestGene.fitness) {
                    bestGene = store.genes.get(i);
                }
            }

            System.out.println("Best Solution: ");
            bestGene.displayChromosome();
            
            bestGene.displaySolution(problem.resources.size(), problem.jobs.size());

            System.out.println("Fitness: " + bestGene.fitness);
            

        }
        catch(FileNotFoundException exc) {
            System.out.println("file not found");
        }

        
    }
    
    
}
