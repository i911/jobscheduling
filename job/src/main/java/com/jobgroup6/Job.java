package com.jobgroup6;
import java.util.LinkedList;

public class Job {

    int id;
    LinkedList<Operation> operations = new LinkedList<>();
    boolean scheduled = false;
    boolean finished;
    int startTime;
    int endTime;

    int makespan;

    //checks if operations previous to current operation are scheduled
    //args: *id of current operation
    public ReturnType previousOperationsScheduled(Operation currentOp) {

        ReturnType result = new ReturnType();
        int numberOfScheduledOperations = 0;

        for(int i = 0; i<currentOp.index; i++) {
            if(this.operations.get(i).scheduled) {
                numberOfScheduledOperations++;
                result.occupiedTime = result.occupiedTime + this.operations.get(i).duration;
            //if this condition is met, it means the prior operation is already scheduled
            } else if(currentOp.index == 0){
                result.previouslyScheduled = true;
                
                break;
            } else {
                break;
            }
        }

        if(numberOfScheduledOperations == currentOp.index) {
            result.previouslyScheduled = true;
        }

            result.numberOfScheduledOperations = numberOfScheduledOperations;
            //result.previouslyScheduled = false;
            return result;

    }

    public void setJobIdForOperations() {
        for(int i = 0; i<this.operations.size(); i++) {
            this.operations.get(i).jobID = this.id;
        }
    }

    public class ReturnType {
        boolean previouslyScheduled;
        int previousResource;
        int occupiedTime;
        int numberOfScheduledOperations;
    }
}