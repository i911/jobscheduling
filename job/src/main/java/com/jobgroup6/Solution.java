package com.jobgroup6;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;


public class Solution {
    
    //fitness
    long duration;
    long startTime;
    long endTime;
    boolean finished;

    //HashMap<ResourceId, List<JobId, OperationIndex>>
    HashMap<Integer, LinkedList<int[]>> schedule;

    //calculates the makespan and corresponding values of the solution -> analogue to calculateFitness() in GeneStore
    public void run(Problem problem) {

        for(Map.Entry<Integer, LinkedList<int[]>> entry : this.schedule.entrySet()) {

            int resourceId = entry.getKey();
            LinkedList<int[]> listOfOperations = entry.getValue();

            Resource resource = problem.resources.get(resourceId);

            for(int i = 0; i<listOfOperations.size(); i++) {
                int[] operationIndex = listOfOperations.get(i);
                Job job = problem.jobs.get(operationIndex[0]);
                Operation operation = job.operations.get(operationIndex[1]);

                resource.scheduledOperations.add(operation);
            }
        }

        int numberOfScheduledJobs = 0;
        while(numberOfScheduledJobs < problem.jobs.size()) {

        for(int i = 0; i<problem.resources.size(); i++) {

            Resource currentResource = problem.resources.get(i);
            LinkedList<Operation> operations = currentResource.scheduledOperations;

            for(int j = 0; j<operations.size(); j++) {

                Operation currentOperation = operations.get(j);
                Job job = problem.jobs.get(currentOperation.jobID);

                if(currentOperation.scheduled) {
                    continue;
                }


                if(job.previousOperationsScheduled(currentOperation).previouslyScheduled) {

                    if(currentOperation.index == 0) {
                        
                        currentOperation.scheduled = true;
                        currentOperation.startTime = currentResource.waitingTime + currentResource.workingTime;
                        currentOperation.endTime = currentOperation.startTime + currentOperation.duration;
                        currentResource.workingTime = currentResource.workingTime + currentOperation.duration;
                        
                        continue;
                    }
                    
                    Operation previousOperation = job.operations.get(currentOperation.index-1);

                    if(previousOperation.endTime > (currentResource.workingTime + currentResource.waitingTime)) {

                        int waitingTime = previousOperation.endTime - (currentResource.workingTime + currentResource.waitingTime);
                        currentResource.waitingTime = currentResource.waitingTime + waitingTime;
                        currentOperation.startTime = currentResource.waitingTime + currentResource.workingTime;
                    }
                    else if(previousOperation.endTime < (currentResource.workingTime + currentResource.waitingTime)) {
                        currentOperation.startTime  = currentResource.waitingTime + currentResource.workingTime;
                    }
                    else {
                        currentOperation.startTime = previousOperation.endTime;
                    }

                    currentResource.workingTime = currentResource.workingTime + currentOperation.duration;
                    currentOperation.scheduled = true;
                    currentOperation.endTime = currentOperation.startTime + currentOperation.duration;

                    if(currentOperation.index == job.operations.size()-1) {
                        numberOfScheduledJobs++;
                        job.endTime = job.operations.getLast().endTime;
                    }
                } 
                else {
                    break;
                }
            }
        }
    }
        
        
    }

    public void displaySolution() {
        for(int i = 0; i<schedule.size(); i++) {
            System.out.println("ResourceId: " + i);
            
            for(int j = 0; j<schedule.get(i).size(); j++) {
                System.out.println("-> " + schedule.get(i).get(j)[0] + schedule.get(i).get(j)[1]);
            }
        } 
    }
}
