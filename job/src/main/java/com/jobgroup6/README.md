# JobScheduling

- Json -> Gson
- combinations -> create one or more intitial solution
- apply optimization algorithm on solutions


# classes:
-> machine/resource
-> job
-> scheduler

# hardConstraints
1. Alle jobs must be scheduled
2. No machine may be assigned more than one operation at any time
3. The order of operations of a job must be maintained

# heuristics
- random:
    choose random fitting job, maintain order of operations!
- greedy:
    earliest end time of operation
    during runtime: add waiting time for an operation and duration of that operation and then choose lowest sum
- shortest processing time:
    shortest duration

# optimization
- evolutionary algrothim:
    intitial solution = individual
    many individuals = population
    

# machine-based
- select resource
- select operation
- run the solution and get waiting time and makespan


# greedy algorithm
list<machines>
list<jobs>
list<sortedOperations>
list<jobId, list<scheduledOperations>> scheduledOperations
result<machine, list<operations>>

1. sort operations regarding shortest processing time
while(!allOperations.scheduled)
2. iterate through machines (for loop)
3. get operations for the specific machine from sorted operations
list<sortedOperationForSpecificMachine>
4. iterate the list
tmp operationWithShortestSumOfWaitingAndProcessingTime
5. if operation.index != 0
    -> if previousOperations.scheduled
        -> sum = waitinTime + ProcessingTime
            if sum < operationWithShortestSumOfWaitingAndProcessingTime
            -> operationWithShortestSumOfWaitingAndProcessingTime = currentOperation
    else
    -> schedule operation in result and scheduledOperations
6. iteration finished
7. schedule operationWithShortestSumOfWaitingAndProcessingTime

# genetic algorithm
- individual
- population

# mutation and runtime
- more jobs per resource -> longer runtime
- swap mutation just picks two jobs of a machine and swaps them
- so probability to achieve a valid solution with swap mutation declines with more jobs per resource

# uselessChilds
- 